from collections import namedtuple
import sre_compile
from unittest import result
from facebook_scraper import get_posts, get_group_info
from pprint import pprint
from pythainlp import sent_tokenize, word_tokenize
import numpy as np

postList = []

class PostInGroupFacebook():
  def __init__(self, usernameFacebook, postText, postImage, postUrl):
    self.usernameFacebook = usernameFacebook
    self.postText = postText
    self.postImage = postImage
    self.postUrl = postUrl

#code group KMUTNB Communioty -> 197822284350539
#get post from group
for post in get_posts(group='197822284350539', pages=1, page_limit=10):
  if post['image'] == None:
    temp_post = PostInGroupFacebook(post['username'], post['post_text'], 'no image', post['post_url'])
    pprint(temp_post)
  else:
    temp_post = PostInGroupFacebook(post['username'], post['post_text'], post['image'], post['post_url'])
    pprint(temp_post)
  # postList.append(temp_post)
  # print(temp_post.printPost())
# pprint(postList)
  